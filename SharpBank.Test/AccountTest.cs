﻿using NUnit.Framework;


namespace SharpBank.Test
{
    [TestFixture]
    public class AccountTest
    {
        [Test]
        public void AmountTransferTest()
        {
            Account checkingAccount = new Account(Account.CHECKING);
            Account savingsAccount = new Account(Account.SAVINGS);

            Customer henry = new Customer("Abi").OpenAccount(checkingAccount).OpenAccount(savingsAccount);

            checkingAccount.Deposit(100.0);
            savingsAccount.Deposit(4000.0);
            savingsAccount.AmountTransfer(checkingAccount, 100.00);

            Assert.AreEqual(3900.00, savingsAccount.SumTransactions());
            Assert.AreEqual(200.00, checkingAccount.SumTransactions());
        }
        [Test]
        public void MaxSavingsAccountInterestCalculationTest()
        {
            Account MaxsavingsAccount = new Account(Account.MAXI_SAVINGS);
            Customer henry = new Customer("Abi").OpenAccount(MaxsavingsAccount);
            MaxsavingsAccount.Deposit(1000.0);
            MaxsavingsAccount.Withdraw(50.0);
            Assert.AreEqual(System.Math.Round(.95,2), System.Math.Round(MaxsavingsAccount.InterestEarned(),2));

        }
        [Test]
        public void InterestEarnedForDaysTest()
        {
            Account SavingsAccount = new Account(Account.SAVINGS);
            Customer henry = new Customer("Thanvi").OpenAccount(SavingsAccount);
            SavingsAccount.Deposit(100.0);
            SavingsAccount.Deposit(100.0);
            SavingsAccount.Withdraw(100.0);
            SavingsAccount.InterestCalculationForDays();
            Assert.AreEqual(System.Math.Round(100.10,2), System.Math.Round(SavingsAccount.SumTransactions(),2));
        }
    }
}
